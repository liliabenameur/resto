import { Component, OnInit } from '@angular/core';
import { User } from '../models/user';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {

  user: User;
  userForm: FormGroup;
  constructor(private fb: FormBuilder,
    private userService: UserService,
    private router: Router) { }

  ngOnInit(): void {
    this.user = new User(0, '','' ,'', '', '','');
    this.userForm = this.fb.group({
      firstName: [''],
      lastName: [''],
      telNumber: [''],
      email: [''],
      password:[''],
      confirmPassword:[''],
    })
  }
  addUser() {
    console.log("this is my form", this.user);
    this.userService.addUser(this.user).subscribe(
      data => {
        console.log("user added successfully");
        this.router.navigate(['/admin']);
      }
    )
  }
}
