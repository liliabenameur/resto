import { Component, OnInit } from '@angular/core';
import { Plat } from '../models/plat';
import { ActivatedRoute } from '@angular/router';
import { PlatService } from '../services/plat.service';

@Component({
  selector: 'app-display-menu',
  templateUrl: './display-menu.component.html',
  styleUrls: ['./display-menu.component.css']
})
export class DisplayMenuComponent implements OnInit {
id:string;
plat:Plat;
  constructor(
    private activateRoute:ActivatedRoute,
    private platService:PlatService
  ) { }

  ngOnInit(): void {
    this.id = this.activateRoute.snapshot.paramMap.get('id');
    this.platService.getPlatById(this.id).subscribe(
      data =>{
        console.log("getted plat in FE",data)
        this.plat = data[0];
      }
    )
  }

}
