export class User{
    constructor (
        public id:number,
        public firstName: string,
        public lastName: string,
        public age: number,
        public email: string,
        public DateBirth:string,
        public password: string
    
    ){}
}