export class User{
    constructor (
        public id:number,
        public firstName: string,
        public lastName: string,
        public password: string,
        public confirmPassword: string,
        public email: string,
        public telNumber: string
    ){}
}