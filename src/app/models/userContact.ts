export class userContact{
    constructor (
        public id:Number,
        public firstName: string,
        public lastName: string,
        public email: string,
        public telNumber: string,
        public subject: string,
        public message: string
    ){}
}