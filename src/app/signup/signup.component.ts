import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MustMatch } from '../models/Validators/mustMatchValidators';
import { UserService } from '../services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
userForm: FormGroup;
  constructor(
    private fb: FormBuilder,
    private userService:UserService,
    private router:Router
  ){ 

    this.userForm = this.fb.group({
      firstName: ['', [Validators.required, Validators.maxLength(10), Validators.minLength(3)]],
      lastName: ['', [Validators.required, Validators.maxLength(15), Validators.minLength(3)]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.maxLength(20), Validators.minLength(8)]],
      telNumber: [''],
      ConfirmPassword: ['', [Validators.required]]
    }, {
      validator: MustMatch('password', 'ConfirmPassword')    } 
  
    )
   }


  ngOnInit(): void {
  }
submitForm(formValue:any){
  console.log("Button clicked");
  console.log("formValue", formValue);
  this.userService.addUser(formValue).subscribe(
    () => {
      console.log("Done");
      this.router.navigate(['/admin']);
    }
  )

}
}
