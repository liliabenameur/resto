import { Component, OnInit } from '@angular/core';
import { Chef } from '../models/chef';
import { PlatService } from '../services/plat.service';
import { ChefService } from '../services/chef.service';

@Component({
  selector: 'app-chefs-part',
  templateUrl: './chefs-part.component.html',
  styleUrls: ['./chefs-part.component.css']
})
export class ChefsPartComponent implements OnInit {
chefs:Chef[];
total:number;

  constructor(
    private platService:PlatService,
    private chefService:ChefService
  ) { }

  ngOnInit(): void {
    this.total = this.platService.calcul(5,8);
    console.log("total",this.total);
    this.total = this.chefService.calcule(10,10);
    console.log("total",this.total);
    // this.chefs= [
    //   {id:1, name: "Lilia", status:"Master chef", img: "assets/img/team/chefs_1.png"},
    //   {id:2, name: "Ahmed",status:"Master chef",  img: "assets/img/team/chefs_2.png"},
    //   {id:3, name: "Khaled", status:"Master chef", img: "assets/img/team/chefs_3.png"}
    // ]
    this.chefService.getChefs().subscribe(
      data => {
        console.log("Datachef", data);
        this.chefs = data.chefs;
      }
    )
  }

}
