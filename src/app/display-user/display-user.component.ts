import { Component, OnInit } from '@angular/core';
import { User } from '../models/user';
import { ActivatedRoute } from '@angular/router';
import { ChefService } from '../services/chef.service';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-display-user',
  templateUrl: './display-user.component.html',
  styleUrls: ['./display-user.component.css']
})
export class DisplayUserComponent implements OnInit {

  id:number;
  user:User;
    constructor(
      private activateRoute:ActivatedRoute,
      private userService:UserService
    ) { }

  ngOnInit(): void {
    this.id = +this.activateRoute.snapshot.paramMap.get('id');
    this.userService.getUserById(this.id).subscribe(
      data =>{
        console.log("User by id = 2 ",data)
        this.user = data;
      }
    )
  }

}
