import { Component, OnInit } from '@angular/core';
import { Chef } from '../models/chef';
import { ActivatedRoute } from '@angular/router';
import { ChefService } from '../services/chef.service';

@Component({
  selector: 'app-display-chef',
  templateUrl: './display-chef.component.html',
  styleUrls: ['./display-chef.component.css']
})
export class DisplayChefComponent implements OnInit {
  id:string;
  chef:Chef;
    constructor(
      private activateRoute:ActivatedRoute,
      private chefService:ChefService
    ) { }

  ngOnInit(): void {
    this.id = this.activateRoute.snapshot.paramMap.get('id');
    this.chefService.getChefById(this.id).subscribe(
      data =>{
        console.log("getted chef in FE",data)
        this.chef = data[0];
      }
    )
  }
  }


