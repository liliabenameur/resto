import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'customEmail'
})
export class CustomEmailPipe implements PipeTransform {

  transform(value: any):any {
           //retourner*a a la place d'une voyelle
         let result='';
         for (let i =0; i< value.length; i++) {
           if (value[i] == 'e'){
             result = result + value[i].replace('e','*');

           }else {
             result = result + value[i];
           }
         }
    return result;
  }

}
