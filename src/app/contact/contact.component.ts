import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  userContactForm: FormGroup;
  constructor(
    private fb: FormBuilder
  ){ 

    this.userContactForm = this.fb.group({
      firstName: ['', [Validators.required, Validators.maxLength(10), Validators.minLength(3)]],
      lastName: ['', [Validators.required, Validators.maxLength(15), Validators.minLength(3)]],
      email: ['', [Validators.required, Validators.email]],
      telNumber: [''],
      subject: ['', [Validators.required, Validators.maxLength(150), Validators.minLength(50)]],
      message: ['', [Validators.required, Validators.maxLength(300), Validators.minLength(100)]],

    }
    )
   }

   ngOnInit(): void {
  }
submitForm(formValue:any){
  console.log("Button clicked");
  console.log("formValue", formValue);

}
}

