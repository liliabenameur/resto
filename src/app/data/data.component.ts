import { Component, OnInit } from '@angular/core';
import { Data } from '@angular/router';
import { TestBed } from '@angular/core/testing';

@Component({
  selector: 'app-data',
  templateUrl: './data.component.html',
  styleUrls: ['./data.component.css']
})
export class DataComponent implements OnInit {
  users:any;
  constructor() { }

  ngOnInit(): void {
    this.users =  [
      {id:1, firstName: "test", lastName:"test",age: 29, email:"test@test.fr",DateBirth:new Date(),password:"test"},
      {id:1, firstName: "test2", lastName:"test",age: 28, email:"test2@test.fr",DateBirth:new Date(),password:"test"},   
      {id:1, firstName: "test3", lastName:"test",age: 27, email:"test3@test.fr",DateBirth:new Date(),password:"test"}, 
        {id:1, firstName: "test4", lastName:"test",age: 26, email:"test4@test.fr",DateBirth:new Date(),password:"test"},
        {id:1, firstName: "test5", lastName:"test",age: 25, email:"test5@test.fr",DateBirth:new Date(),password:"test"},
   
   ] 
  }

}
