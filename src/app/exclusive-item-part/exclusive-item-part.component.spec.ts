import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExclusiveItemPartComponent } from './exclusive-item-part.component';

describe('ExclusiveItemPartComponent', () => {
  let component: ExclusiveItemPartComponent;
  let fixture: ComponentFixture<ExclusiveItemPartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExclusiveItemPartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExclusiveItemPartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
