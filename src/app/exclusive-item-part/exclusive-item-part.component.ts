import { Component, OnInit } from '@angular/core';
import { PlatService } from '../services/plat.service';
import { Plat } from '../models/plat';

@Component({
  selector: 'app-exclusive-item-part',
  templateUrl: './exclusive-item-part.component.html',
  styleUrls: ['./exclusive-item-part.component.css']
})
export class ExclusiveItemPartComponent implements OnInit {
result: number;
plats:Plat[];
  constructor(private platSevice: PlatService) { }

  ngOnInit(): void {
    this.platSevice.getPlats().subscribe(
      data =>{
        this.plats = data.plats;
      }
    )
  }

}
