import { Component, OnInit } from '@angular/core';
import { Chef } from '../models/chef';
import { ChefService } from '../services/chef.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-edit-chef',
  templateUrl: './edit-chef.component.html',
  styleUrls: ['./edit-chef.component.css']
})
export class EditChefComponent implements OnInit {
  ch: Chef;
  id:string;

 
    constructor(
      private chefService:ChefService,
      private activatedRoute: ActivatedRoute,
      private router:Router
    ) { }
  
    ngOnInit(): void {
      this.id=  this.activatedRoute.snapshot.paramMap.get('id');
      this.chefService.getChefById(this.id).subscribe(
        data =>{
          this.ch = data[0];
          console.log("this my menu",this.ch)
        }
      )  }
  editChef(chef:any){
    this.chefService.updateChef(this.ch).subscribe(
      () =>{
        this.router.navigate(['/admin']);
      }
    )
  }

}
