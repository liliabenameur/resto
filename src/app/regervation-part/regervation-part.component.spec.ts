import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegervationPartComponent } from './regervation-part.component';

describe('RegervationPartComponent', () => {
  let component: RegervationPartComponent;
  let fixture: ComponentFixture<RegervationPartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegervationPartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegervationPartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
