import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Plat } from '../models/plat';
import { PlatService } from '../services/plat.service';
import { Chef } from '../models/chef';
import { ChefService } from '../services/chef.service';
import { User } from '../models/user';
import { UserService } from '../services/user.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
plats : Plat[];
chefs : Chef[];
users : User[];
message : string;
authSubs: Subscription;
isUserAuthenticated = false;

  constructor(
    private router:Router,
    private platService:PlatService,
    private chefService:ChefService,
    private userService:UserService) { }

  ngOnInit(): void {
    this.platService.getPlats().subscribe(
      data => {
        console.log("Data", data);
        this.plats = data.plats;
        this.message = data.message;
        this.isUserAuthenticated = this.userService.isUserAuth();

      }
    )
    this.chefService.getChefs().subscribe(
      data =>{
        this.chefs = data.chefs;
        this.message = data.message;
      }
    )
    this.userService.getUsers().subscribe(
      data =>{
        this.users = data.users;
        this.message = data.message;

      }
    )
  }
goToAddMenu(){
  this.router.navigate(['/addPlat']);
}
updatePlats(plats: Plat[]){
  this.plats = plats;
}
updateChefs(chefs: Chef[]){
  this.chefs = chefs;
}
updateUser(users: User[]){
  this.users = users;
}
goToAddChef(){
  this.router.navigate(['/addChef']);
}
goToAddUser(){
  this.router.navigate(['/addUser']);
}
}
