import { Component, OnInit, Input, Output,EventEmitter } from '@angular/core';
import { User } from '../models/user';
import {  } from 'protractor';
import { Router } from '@angular/router';
import { UserService } from '../services/user.service';


@Component({
  selector: 'app-user-admin',
  templateUrl: './user-admin.component.html',
  styleUrls: ['./user-admin.component.css']
})
export class UserAdminComponent implements OnInit {

  @Input() user:User;
  @Output() newUsers: EventEmitter<User[]> = new EventEmitter();

  constructor(private router:Router,
    private userService:UserService) { }

  ngOnInit(): void {
  }
  displayUser(id:number){
    this.router.navigate([`/display-user/${id}`]);
 
  }
  deleteUser(id:number){
    console.log("please delee this menu id =",id);
    // 3 chefs
    this.userService.deleteUser(id).subscribe( 
      data =>{
       
        console.log("datadelete",data)   ;
    }
    );
    //2 chefs
    this.userService.getUsers().subscribe(
      data =>{
        this.newUsers.emit(data.users);
      }
    )
  }
  editUser(id:number){
    this.router.navigate([`/editUser/${id}`])
  }

}
