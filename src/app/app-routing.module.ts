import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';
// import { AboutComponent } from './component/about/about.component';
import { ContactComponent } from './contact/contact.component';
import { AdminComponent } from './admin/admin.component';
import { AddPlatComponent } from './add-plat/add-plat.component';
import { DisplayMenuComponent } from './display-menu/display-menu.component';
import { ChefComponent } from './chef/chef.component';
import { AboutComponent } from './about/about.component';
import { DisplayChefComponent } from './display-chef/display-chef.component';
import { AddChefComponent } from './add-chef/add-chef.component';
import { EditMenuComponent } from './edit-menu/edit-menu.component';
import { EditChefComponent } from './edit-chef/edit-chef.component';
import { DisplayUserComponent } from './display-user/display-user.component';
import { EditUserComponent } from './edit-user/edit-user.component';
import { AddUserComponent } from './add-user/add-user.component';
import { AuthGuardService } from './services/auth-guard.service';
import { DataComponent } from './data/data.component';


const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'sign-up', component: SignupComponent },
  { path: 'login', component: LoginComponent },
  { path: 'contact', component: ContactComponent },
  { path: 'about', component: AboutComponent },
  { path: 'admin', component: AdminComponent, canActivate:[AuthGuardService]},
  { path: 'addPlat', component: AddPlatComponent,canActivate:[AuthGuardService] },
  { path: 'display-menu/:id', component: DisplayMenuComponent },
  { path: 'chef', component: ChefComponent },
  { path: 'display-chef/:id', component: DisplayChefComponent },
  { path: 'addChef', component: AddChefComponent },
  { path: 'editMenu/:id', component: EditMenuComponent },
  { path: 'editChef/:id', component: EditChefComponent },
  { path: 'display-user/:id', component: DisplayUserComponent },
  { path: 'editUser/:id', component: EditUserComponent },
  { path: 'addUser', component: AddUserComponent },
  {path:'data' , component: DataComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
