import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntroVideoBgComponent } from './intro-video-bg.component';

describe('IntroVideoBgComponent', () => {
  let component: IntroVideoBgComponent;
  let fixture: ComponentFixture<IntroVideoBgComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntroVideoBgComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntroVideoBgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
