import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[appBackground]'
})
export class BackgroundDirective {

  constructor(private el: ElementRef) {
    // el.nativeElement.style.backgroundColor = 'red';
   }
   @HostListener('mouseenter') onMouseEnter() {
this.changeColor("yellow");
  }
@HostListener('mouseleave') onMouseLeave() {
this.changeColor("red");
}
changeColor(color:string){
  this.el.nativeElement.style.backgroundColor = color;

}
}
