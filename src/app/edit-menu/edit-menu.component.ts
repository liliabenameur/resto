import { Component, OnInit } from '@angular/core';
import { Plat } from '../models/plat';
import { PlatService } from '../services/plat.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-edit-menu',
  templateUrl: './edit-menu.component.html',
  styleUrls: ['./edit-menu.component.css']
})
export class EditMenuComponent implements OnInit {
id:string;
menu:Plat;
  constructor(
    private platService:PlatService,
    private activatedRoute: ActivatedRoute,
    private router:Router
  ) { }

  ngOnInit(): void {
    this.id=  this.activatedRoute.snapshot.paramMap.get('id');
this.platService.getPlatById(this.id).subscribe(
  data =>{
    this.menu = data[0];
    console.log("this my menu",this.menu)
  }
)  }
editMenu(p:any){
  this.platService.editPlat(this.menu).subscribe(
    () =>{
      this.router.navigate(['/admin']);
    }
  )
}

}
