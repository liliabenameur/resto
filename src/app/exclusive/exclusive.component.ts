import { Component, OnInit } from '@angular/core';
import { Plat} from '../models/plat';

@Component({
  selector: 'app-exclusive',
  templateUrl: './exclusive.component.html',
  styleUrls: ['./exclusive.component.css']
})
export class ExclusiveComponent implements OnInit {
  plats:Plat[];

  constructor() { }

  ngOnInit(): void {
    // this.plats= [
    //   {_id:1, name: "Plat Ojja", price: 10, description: "Ojja escalope + frites", img: "assets/img/food_item/ojja.jpg"},
    //   {_id:2, name: "Plat Spaghetti", price: 8, description: "Spaghetti + frites", img: "assets/img/food_item/spaghetti.jpg"},
    //   {_id:3, name: "Plat Mloukhia", price: 11, description: "Mloukhia + frites", img: "assets/img/food_item/mloukhia.jpg"}
    // ]
  }

}
