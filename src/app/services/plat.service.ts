import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Plat } from '../models/plat';
import { $ } from 'protractor';

@Injectable({
  providedIn: 'root'
})
export class PlatService {
  private platUrl = 'http://localhost:3000/';
  constructor(private http: HttpClient) { }
  calcul(a: number, b: number): number {
    return (a + b);
  }
  //get all plats
  //api/plats
  getPlats() {
    const url=`${this.platUrl}api/allplats`;

    return this.http.get<{message: string, plats:Plat[]}>(url);
  }
  //get plat by id
  //api/plats/:id
  getPlatById(id:string): Observable<Plat> {
    const url = `${this.platUrl}api/plats/${id}`;
    return this.http.get<Plat>(url);
  }

  //delete plat by id
  //api/plats/:id
  deletePlat(id: string): Observable<Plat> {
    const url = `${this.platUrl}api/plats/${id}`;
    return this.http.delete<Plat>(url);
  }
  //add plat
  //api/ plats
  addPlat(plat:Plat, image: File):Observable<Plat>{
    const url=`${this.platUrl}api/plats`;
    let formData = new FormData();
    formData.append('name',plat.name);
    formData.append('price',String(plat.price));
    formData.append('description',plat.description);
    formData.append('img',image, plat.name);

    console.log("url of add plat",url);
    
    return this.http.post<Plat>(url,formData)
  }


  //edit plat
  //api/plat
  editPlat(plat:Plat): Observable<Plat>{
    const url=`${this.platUrl}api/plats/${plat._id}`;
    return this.http.put<Plat>(url, plat);

  }
}
