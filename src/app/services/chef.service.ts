import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Chef } from '../models/chef';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ChefService {
  private chefUrl = "http://localhost:3000/";
  constructor(private http:HttpClient) { }
  calcule(a:number,b:number): number {
    return (a+b);
  }
 
//get all chefs
//api/chefs
getChefs() {
  const url=`${this.chefUrl}api/allchefs`;
  return this.http.get<{message: string, chefs:Chef[]}>(url);

  }
   //get Chef by id
  //api/Chefs/:id
  getChefById(id: string): Observable<Chef> {
    const url = `${this.chefUrl}api/chefs/${id}`;
    return this.http.get<Chef>(url);
  }

  //delete Chef by id
  //api/Chefs/:id
  deleteChef(id: string): Observable<Chef> {
    const url = `${this.chefUrl}api/chefs/${id}`;
    return this.http.delete<Chef>(url);
  }
  //add Chef
  //api/ Chefs
  addChef(chef: Chef, image: File): Observable<Chef> {
    const url=`${this.chefUrl}api/chefs`;
    console.log("url of add chef",url);

    let formData = new FormData();
    formData.append('name',chef.name);
    formData.append('description',chef.status);
    formData.append('img',image,chef.name);

    console.log("url of add chef",url);
    
    return this.http.post<Chef>(url,formData)

  }
  // update chef 
  //api/chef
  updateChef(chef: Chef): Observable<Chef>{
    const url=`${this.chefUrl}api/chefs/${chef._id}`;
    return this.http.put<Chef>(url, chef);

  }
  
}
