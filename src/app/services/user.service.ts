import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { User } from '../models/user';
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class UserService {
private userUrl= "http://localhost:3000/";
public token: string;
  private tokenTimer: any;

  private authStatusListener = new Subject<boolean>();
  private isUserAuthenticated = false;

  constructor(private http:HttpClient,
    private router:Router) { }
    //get all Users
  //api/Users
  getUsers() {
    const url=`${this.userUrl}api/allusers`;
    return this.http.get<{message: string, users:User[]}>(url);
  }
  
  //get User by id
  //api/Users/:id
  getUserById(id: number): Observable<User> {
    const url = `${this.userUrl}/ ${id}`;
    return this.http.get<User>(url);
  }

  //delete User by id
  //api/Users/:id
  deleteUser(id: number): Observable<User> {
    const url = `${this.userUrl}/ ${id}`;
    return this.http.delete<User>(url);
  }
  //add user
  addUser(user:User){
    const url = `${this.userUrl}api/users/signup`;
    console.log("URL signup", url);
    return this.http.post(url, user);
  }
  // update User 
  //api/User
  updateUser(user: User): Observable<User>{
    return this.http.put<User>(this.userUrl, user);

  }
  getAllUsers(): Observable<User[]>{
    return this.http.get<User[]>(this.userUrl)
  }
  //edit User
  //api/User
  editUser(User:User){
    return this.http.put<User>(this.userUrl, User);

  }
  login(user) {
    const url = `${this.userUrl}api/users/login`;
    this.http.post<{ token: string, expiresIn: number }>(url, user).subscribe(
      res => {
        
        const token = res.token;
        this.token = token;
        if (token) {
          const expireInDuration = res.expiresIn;

          this.setAuthTimer(expireInDuration);
          this.isUserAuthenticated = true;
          this.authStatusListener.next(true);
          const now = new Date();
          const expirationDate = new Date(now.getTime() + expireInDuration * 1000);
          this.saveAuthData(token, expirationDate);
          this.router.navigate(['/admin']);
        }
      })


  }
  getToken() {
    return this.token;
  }

  getAuthStatusListener() {
    return this.authStatusListener.asObservable();
  }

  isUserAuth() {
    return this.isUserAuthenticated;
  }
  autoAuthUser() {
    const token = localStorage.getItem('token');
    const expirationDate = localStorage.getItem('expiration');
    if (!token || !expirationDate) {
      return;
    }
    const now = new Date();
    const expiresIn = new Date(expirationDate).getTime() - now.getTime();

    console.log("expiresIn", expiresIn);


    if (expiresIn) {
      this.token = token;
      this.isUserAuthenticated = true;
      this.setAuthTimer(expiresIn / 1000);
      this.authStatusListener.next(true);
    }
  }

  logout() {
    this.clearAuthData();
    this.isUserAuthenticated = false;
    this.authStatusListener.next(false);
    this.router.navigate(['/']);
    clearTimeout(this.tokenTimer);

  }
private setAuthTimer(duration: number) {
    console.log('Set Timer', duration);

    this.tokenTimer = setTimeout(() => {
      this.logout();
    }, duration * 1000);
  }

  private saveAuthData(token: string, expirationDate: Date) {
    localStorage.setItem('token', token);
    localStorage.setItem('expiration', expirationDate.toISOString());
  }

  private clearAuthData() {
    localStorage.removeItem('token');
    localStorage.removeItem('expiration');
  }


}
