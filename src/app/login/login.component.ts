import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  model: any={};
  loginForm: FormGroup;
  constructor(private formBuilder:FormBuilder, private userService:UserService) { 
    this.loginForm = this.formBuilder.group({
      email: [''],
      password: ['']
    })
  }

  ngOnInit() {
  }

  login(form:any){
    console.log('This model', this.model);
    this.userService.login(this.model);
    
  }
  

}