import { Component, OnInit } from '@angular/core';
import { Chef } from '../models/chef';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ChefService } from '../services/chef.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-chef',
  templateUrl: './add-chef.component.html',
  styleUrls: ['./add-chef.component.css']
})
export class AddChefComponent implements OnInit {
  ch: Chef;
  chefForm: FormGroup;
  menuForm: FormGroup;
  imagePreview: string;
  constructor(private fb: FormBuilder,
    private chefService: ChefService,
    private router: Router) { }

  ngOnInit(): void {
    this.ch = new Chef('', '', '', '');
    this.chefForm = this.fb.group({
      name: [''],
      status: [''],
      img: ['']
    })
  }
  addChef() {
    console.log("this is my form", this.ch);
    this.chefService.addChef(this.ch,this.chefForm.value.img).subscribe(
      data => {
        console.log("chef added successfully");
        this.router.navigate(['/admin']);
      }
    )
  }
  onImageSelected(event: Event) {
    const file = (event.target as HTMLInputElement).files[0];
    this.chefForm.patchValue({img: file});
    this.chefForm.updateValueAndValidity();
    console.log("This is my file", file);
    console.log("This is my form", this.chefForm);
    const reader = new FileReader();
    reader.onload = () => {
      this.imagePreview =  reader.result as string
    };
    reader.readAsDataURL(file);
    
  }
}

