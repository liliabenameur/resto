import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-single-blog-item',
  templateUrl: './single-blog-item.component.html',
  styleUrls: ['./single-blog-item.component.css']
})
export class SingleBlogItemComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
