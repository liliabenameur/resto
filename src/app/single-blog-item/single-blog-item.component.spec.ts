import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleBlogItemComponent } from './single-blog-item.component';

describe('SingleBlogItemComponent', () => {
  let component: SingleBlogItemComponent;
  let fixture: ComponentFixture<SingleBlogItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SingleBlogItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleBlogItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
