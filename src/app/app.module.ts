import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { ExclusiveComponent } from './exclusive/exclusive.component';
import { IntroVideoBgComponent } from './intro-video-bg/intro-video-bg.component';
import { FoodMenuComponent } from './component/food-menu/food-menu.component';
import { ChefsPartComponent } from './chefs-part/chefs-part.component';
import { RegervationPartComponent } from './regervation-part/regervation-part.component';
import { ReviewPartComponent } from './review-part/review-part.component';
import { ExclusiveItemPartComponent } from './exclusive-item-part/exclusive-item-part.component';
import { FooterAreaComponent } from './footer-area/footer-area.component';
import { SingleBlogItemComponent } from './single-blog-item/single-blog-item.component';
import { SignupComponent } from './signup/signup.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { ContactComponent } from './contact/contact.component';
import { ReactiveFormsModule, FormsModule} from '@angular/forms';
import { PlatComponent } from './plat/plat.component';
import { AdminComponent } from './admin/admin.component';
import { AddPlatComponent } from './add-plat/add-plat.component';
import { HomeMenuComponent } from './home-menu/home-menu.component';
import { DisplayMenuComponent } from './display-menu/display-menu.component';
import { ChefComponent } from './chef/chef.component';
import { PlatService } from './services/plat.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BannerComponent } from './banner/banner.component';
import { AboutComponent } from './about/about.component';
import { ChefService } from './services/chef.service';
import { ChefAdminComponent } from './chef-admin/chef-admin.component';
import { DisplayChefComponent } from './display-chef/display-chef.component';
import { AddChefComponent } from './add-chef/add-chef.component';
import { EditMenuComponent } from './edit-menu/edit-menu.component';
import { EditChefComponent } from './edit-chef/edit-chef.component';
import { UserAdminComponent } from './user-admin/user-admin.component';
import { DisplayUserComponent } from './display-user/display-user.component';
import { EditUserComponent } from './edit-user/edit-user.component';
import { UserService } from './services/user.service';
import { AddUserComponent } from './add-user/add-user.component';
import { AuthInterceptor } from './services/auth-interceptor.service';
import { AuthGuardService } from './services/auth-guard.service';
import { DataComponent } from './data/data.component';
import { CustomEmailPipe } from './custom-email.pipe';
import { BackgroundDirective } from './directives/background.directive';
import { ReservePipe } from './reserve.pipe';
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    BannerComponent,
    ExclusiveComponent,
    AboutComponent,
    IntroVideoBgComponent,
    FoodMenuComponent,
    ChefsPartComponent,
    RegervationPartComponent,
    ReviewPartComponent,
    ExclusiveItemPartComponent,
    FooterAreaComponent,
    SingleBlogItemComponent,
    SignupComponent,
    HomeComponent,
    LoginComponent,
    ContactComponent,
    PlatComponent,
    AdminComponent,
    AddPlatComponent,
    HomeMenuComponent,
    DisplayMenuComponent,
    ChefComponent,
    ChefAdminComponent,
    DisplayChefComponent,
    AddChefComponent,
    EditMenuComponent,
    EditChefComponent,
    UserAdminComponent,
    DisplayUserComponent,
    EditUserComponent,
    AddUserComponent,
    DataComponent,
    CustomEmailPipe,
    BackgroundDirective,
    ReservePipe
  ],
  imports: [
    ReactiveFormsModule,
    FormsModule,
    BrowserModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [PlatService,ChefService,UserService, AuthGuardService, { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
