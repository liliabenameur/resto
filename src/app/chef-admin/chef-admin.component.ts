import { Component, OnInit, Input, Output,EventEmitter} from '@angular/core';
import { Chef } from '../models/chef';
import { Router } from '@angular/router';
import { ChefService } from '../services/chef.service';
import {  } from 'protractor';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-chef-admin',
  templateUrl: './chef-admin.component.html',
  styleUrls: ['./chef-admin.component.css']
})
export class ChefAdminComponent implements OnInit {
chefs:any;

chefForm: FormGroup;
  @Input() chef:Chef;
  @Output() newChefs: EventEmitter<Chef[]> = new EventEmitter();

  constructor(private router:Router,
    private chefService:ChefService) { }

  ngOnInit(): void {
  }
  displayChef(id:string){
    this.router.navigate([`/display-chef/${id}`]);
 
  }
  deleteChef(id:string){
    console.log("please delete this menu id =",id);
    // 3 chefs
    this.chefService.deleteChef(id).subscribe( 
      data =>{
        this.chefs = data;
        console.log("datadelete",data)   ;
    }
    );
    //2 chefs
    this.chefService.getChefs().subscribe(
      data =>{
        this.newChefs.emit(data.chefs);
      }
    )
  }
  editChef(id:string){
    this.router.navigate([`/editChef/${id}`])
  }
}
