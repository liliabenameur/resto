import { Component, OnInit, Input, Output,EventEmitter } from '@angular/core';
import { Plat } from '../models/plat';
import { Router } from '@angular/router';
import { DisplayMenuComponent } from '../display-menu/display-menu.component';
import { PlatService } from '../services/plat.service';
import { Observable } from 'rxjs';





@Component({
  selector: 'app-plat',
  templateUrl: './plat.component.html',
  styleUrls: ['./plat.component.css']
})
export class PlatComponent implements OnInit {
@Input() p:Plat;
@Output() newPlats: EventEmitter<Plat[]> = new EventEmitter();
  plats: any;
  
  constructor(private router: Router,
    private platService:PlatService) { }
    

  ngOnInit(): void {
  }
    displayMenu(id:string){
      this.router.navigate([`/display-menu/${id}`]);
   
    }
    deleteMenu(id:string){
      console.log("please delee this menu id =",id);
      // 3 plats
      this.platService.deletePlat(id).subscribe( 
        data =>{
          this.plats =data;
          console.log("datadelete",data)   ;
      }
      );
      //2 plats
      this.platService.getPlats().subscribe(
        data =>{
          this.newPlats.emit(data.plats);
        }
      )
    }
editMenu(id:string){
  this.router.navigate([`/editMenu/${id}`])
}
  }