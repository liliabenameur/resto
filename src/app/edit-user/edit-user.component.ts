import { Component, OnInit } from '@angular/core';
import { User } from '../models/user';
import { UserService } from '../services/user.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {
  id:number;
  user:User;
    constructor(
      private userService:UserService,
      private activatedRoute: ActivatedRoute,
      private router:Router
    ) { }
  
    ngOnInit(): void {
      this.id= + this.activatedRoute.snapshot.paramMap.get('id');
  this.userService.getUserById(this.id).subscribe(
    data =>{
      this.user = data;
      console.log("this my user",this.user)
    }
  )  }
  editUser(p:any){
    this.userService.updateUser(this.user).subscribe(
      () =>{
        this.router.navigate(['/admin']);
      }
    )
  }

}
