const express = require('express');
const router = express.Router();
const User = require('../models/user');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

router.post('/signup', (req, res) => {
    // Traitement Signup

    bcrypt.hash(req.body.password, 10).then(
        hash => {

            const user = new User({
                email: req.body.email,
                password: hash,
                firstName: req.body.firstName,
                lastName: req.body.lastName
            });

            user.save()
                .then(
                    result => {
                        res.status(201).json({
                            message: "User added successfully",
                            result: result
                        })
                    }
                ).catch(err => {
                    res.status(500).json({
                        error: err
                    })
                })

        }
    )



});
router.post('/login', (req, res) => {
    // Traitement Login

    let gettedUser;
    User.findOne({ email: req.body.email }).then(
            user => {
                if (!user) {
                    // 401 : authentification failed
                    // 404 : not found
                    res.status(401).json({
                        message: "email invalid:Auth failed"
                    })
                }
                gettedUser = user;
                return bcrypt.compare(req.body.password, gettedUser.password)
            })
        .then(
            result => {
                if (!result) {
                    return res.status(401).json({
                        message: 'Password invalid: Auth failed'
                    })
                }
                //generate Token
                const token = jwt.sign({ email: gettedUser.email, userId: gettedUser._id }, 'secret_key', { expiresIn: '1h' });
                //return reponse in FE 
                res.status(200).json({
                    token: token,
                    expiresIn: 3600
                })
            }
        )
        .catch(err => {
            console.log("err", err);
            res.status(401).json({
                message: 'error:Auth failed'
            })
        })
});
module.exports = router;