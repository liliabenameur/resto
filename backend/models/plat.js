const mongoose = require('mongoose');

const platSchema = mongoose.Schema({
    name: String,
    price: Number,
    description: String,
    img: String
});

const plat = mongoose.model('Plat', platSchema);

module.exports = plat;