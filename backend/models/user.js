const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');


const userSchema = mongoose.Schema({
    email: { type: String, require: true, unique: true },
    password: String,
    firstName: String,
    lastName: String


});
userSchema.plugin(uniqueValidator);
const user = mongoose.model('User', userSchema);

module.exports = user;